package de.mariusfreitag.gradecalc.common;

interface RessourceResolver {
    double resolve(int index);
}
