package de.mariusfreitag.gradecalc.common;

import android.content.Context;
import android.support.v7.app.AlertDialog;

import de.mariusfreitag.gradecalc.R;

public class FileAccessFailedDialog {
    public void show(Context context) {
        new Exception().printStackTrace();

        new AlertDialog.Builder(context)
                .setMessage(context.getString(R.string.file_access_failed))
                .setPositiveButton(context.getString(R.string.exit), (dialog, which) -> System.exit(1))
                .setCancelable(false)
                .show();
    }
}
