package de.mariusfreitag.gradecalc.common;


import android.os.Environment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

import de.mariusfreitag.gradecalc.model.Profile;

public class Storage {

    private static final File profileConfigFile = new File(Environment.getExternalStorageDirectory(), "GradeCalc/profiles.json");
    private static final File importAskedFile = new File(Environment.getExternalStorageDirectory(), "GradeCalc/importasked");
    private static Profile profile = null;

    private static void assertStorageWritable() throws StorageException {
        String state = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(state)) {
            throw new StorageException("Storage is not writable!");
        }
    }

    private static void assertStorageReadable() throws StorageException {
        String state = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(state) && !Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            throw new StorageException("Storage is not readable!");
        }
    }

    public static synchronized Profile getProfile() throws StorageException {
        assertStorageReadable();

        if (profile == null) {
            if (!profileConfigFile.exists()) {
                profile = Profile.getDefault();
            } else {
                try (FileInputStream inputStream = new FileInputStream(profileConfigFile)) {
                    profile = new Gson().fromJson(new JsonReader(new InputStreamReader(inputStream)), Profile.class);
                } catch (Exception e) {
                    throw new StorageException(e.getMessage());
                }
            }
        }

        return profile;
    }

    public static synchronized void setProfile(Profile newProfile) throws StorageException {
        assertStorageWritable();

        if (!profileConfigFile.getParentFile().exists() && !profileConfigFile.getParentFile().mkdirs()) {
            throw new StorageException("Config directory could not be created!");
        }

        profile = newProfile;

        try (FileOutputStream outputStream = new FileOutputStream(profileConfigFile)) {
            outputStream.write(new GsonBuilder().setPrettyPrinting().create().toJson(profile).getBytes());
        } catch (Exception e) {
            throw new StorageException(e.getMessage());
        }
    }

    public static synchronized boolean isImportAsked() {
        return importAskedFile.exists();
    }

    public static synchronized void setImportAsked() {
        try (FileOutputStream outputStream = new FileOutputStream(importAskedFile)) {
            outputStream.write("true".getBytes());
        } catch (Exception ignored) {
        }
    }

    public static class StorageException extends Exception {
        StorageException(String message) {
            super(message);
        }
    }
}
