package de.mariusfreitag.gradecalc.common;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import de.mariusfreitag.gradecalc.model.Module;
import de.mariusfreitag.gradecalc.model.Submodule;
import de.mariusfreitag.gradecalc.model.WeightedValue;

public class GradeCalculator {

    private final RessourceResolver gradeIndexResolver;

    public GradeCalculator(RessourceResolver gradeIndexResolver) {
        this.gradeIndexResolver = gradeIndexResolver;
    }

    /**
     * Cuts off all decimal places except one
     * <p>
     * Example: 1.19 -> 1.1
     */
    double cutOffDecimals(double input) {
        return ((int) (input * 10)) / 10.0;
    }

    WeightedValue calculateModuleGrade(Module module) {
        WeightedValue value = new WeightedValue();

        value.setWeight(
                module.getSubmodules().stream()
                        .filter((s) -> s.getGradeIndex() > 0)
                        .mapToDouble(Submodule::getCredits)
                        .sum()
        );

        // If the whole module is without credits, just add all grades with equal weight
        if (value.getWeight() == 0) {
            value.setValue(module.getSubmodules().stream()
                    .mapToDouble(submodule -> gradeIndexResolver.resolve(submodule.getGradeIndex()))
                    .sum() / module.getSubmodules().size());
        } else {
            value.setValue(module.getSubmodules().stream()
                    .mapToDouble(submodule -> gradeIndexResolver.resolve(submodule.getGradeIndex()) * submodule.getCredits())
                    .sum() / value.getWeight());
        }

        value.setValue(cutOffDecimals(value.getValue()));

        return value;
    }

    public double calculateGradeAverage(List<Module> modules) {
        double weights = 0;
        double weightedSum = 0;

        for (Module module : modules) {
            WeightedValue moduleValue = calculateModuleGrade(module);

            if (moduleValue.getValue() > 0) {
                weights += moduleValue.getWeight();
                weightedSum += moduleValue.getValue() * moduleValue.getWeight();
            }
        }

        double average = 0;

        if (weights > 0) {
            average = weightedSum / weights;
        }

        for (Module module : modules.stream().filter((m) -> m.getAdditionalInfluence() > 0).collect(Collectors.toList())) {
            WeightedValue moduleValue = calculateModuleGrade(module);

            if (moduleValue.getValue() > 0) {
                if (average > 0) {
                    average = cutOffDecimals(average) * (1 - module.getAdditionalInfluence())
                            + moduleValue.getValue() * module.getAdditionalInfluence();
                } else {
                    average = moduleValue.getValue();
                }
            }
        }

        return average;
    }

    public double projectGradeAverage(List<Module> modules, int projectionIndex) {
        List<Module> projectedModules = new LinkedList<>();

        // Copy each module and insert the projection
        for (Module oldModule : modules) {
            List<Submodule> projectedSubmodules = new LinkedList<>();

            for (Submodule oldSubmodule : oldModule.getSubmodules()) {
                Submodule newSubmodule = new Submodule(oldSubmodule.getName(), oldSubmodule.getCredits());

                // If the grade is not set, insert the projection
                newSubmodule.setGradeIndex(
                        gradeIndexResolver.resolve(oldSubmodule.getGradeIndex()) > 0
                                ? oldSubmodule.getGradeIndex()
                                : projectionIndex);

                projectedSubmodules.add(newSubmodule);
            }

            projectedModules.add(Module.createModule(oldModule.getName(), projectedSubmodules, oldModule.getAdditionalInfluence()));
        }

        return calculateGradeAverage(projectedModules);
    }
}
