package de.mariusfreitag.gradecalc.common;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.Objects;

import de.mariusfreitag.gradecalc.R;

public class GradeIndexResolver implements RessourceResolver {

    private final ArrayAdapter<CharSequence> arrayAdapter;

    public GradeIndexResolver(Context context) {
        this.arrayAdapter = ArrayAdapter.createFromResource(context, R.array.grades, android.R.layout.simple_spinner_item);
    }

    @Override
    public double resolve(int index) {
        try {
            return Double.parseDouble(Objects.requireNonNull(arrayAdapter.getItem(index)).toString());
        } catch (Exception e) {
            return 0;
        }
    }
}
