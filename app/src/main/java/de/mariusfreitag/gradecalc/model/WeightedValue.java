package de.mariusfreitag.gradecalc.model;

public class WeightedValue {

    private double weight = 0;
    private double value = 0;

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
