package de.mariusfreitag.gradecalc.model;

public class Submodule {
    private String name;
    private double credits;
    private int gradeIndex = 0;

    public Submodule(String name, double credits) {
        setName(name);
        setCredits(credits);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCredits() {
        return credits;
    }

    public void setCredits(double credits) {
        this.credits = credits;
    }

    public int getGradeIndex() {
        return gradeIndex;
    }

    public void setGradeIndex(int gradeIndex) {
        this.gradeIndex = gradeIndex;
    }
}
