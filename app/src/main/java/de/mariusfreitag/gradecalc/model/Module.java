package de.mariusfreitag.gradecalc.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Module {
    private String name;
    private List<Submodule> submodules = new LinkedList<>();
    private double additionalInfluence = 0.0;

    private Module() {

    }

    public static Module createModule(String name, List<Submodule> submodules, double additionalInfluence) {
        Module module = new Module();
        module.setName(name);
        module.setSubmodules(submodules);
        module.setAdditionalInfluence(additionalInfluence);
        return module;
    }

    public static Module createModule(String name, Submodule... submodules) {

        return Module.createModule(name, new LinkedList<>(Arrays.asList(submodules)), 0);
    }

    public static Module createModule(String name, int credits) {
        return Module.createModule(name, new Submodule("", credits));
    }

    public static Module createModule(String name, int credits, double additionalInfluence) {
        return Module.createModule(name, new LinkedList<>(Collections.singletonList(new Submodule("", credits))), additionalInfluence);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Submodule> getSubmodules() {
        return submodules;
    }

    private void setSubmodules(List<Submodule> submodules) {
        this.submodules = submodules;
    }

    public double getAdditionalInfluence() {
        return additionalInfluence;
    }

    public void setAdditionalInfluence(double additionalInfluence) {
        this.additionalInfluence = additionalInfluence;
    }
}
