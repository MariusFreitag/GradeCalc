package de.mariusfreitag.gradecalc.model;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class Profile {
    private List<Module> modules = new LinkedList<>();

    private Profile() {
    }

    public static Profile getDefault() {
        return new Profile();
    }

    public static Profile getTinf() {
        Profile profile = new Profile();
        profile.getModules().addAll(Arrays.asList(
                // Semester I
                Module.createModule("Praxis I", 0),
                Module.createModule("Theoretische Informatik I", 5),
                Module.createModule("Technische Informatik I", 5),
                Module.createModule("Webengineering I", 3),

                // Semester II
                Module.createModule("Mathematik I", 8),
                Module.createModule("Theoretische Informatik II", 5),
                Module.createModule("Programmieren", 9),
                Module.createModule("Schlüsselqualifikationen I", 5),
                Module.createModule("Projekt AI", 5),
                Module.createModule("Schlüsselqualifikationen II", 5),

                // Semester III
                Module.createModule("Theoretische Informatik III", 6),
                Module.createModule("Kommunikations- und Netztechnik I", 5),

                // Semester IV
                Module.createModule("Praxis II", 20),
                Module.createModule("Mathematik II", 6),
                Module.createModule("Software Engineering I", 9),
                Module.createModule("Datenbanken I", 6),
                Module.createModule("Technische Informatik II", 8),
                Module.createModule("Techniken der Informatik", 5),
                Module.createModule("Systemtheorie und Softwareengineering", 5),

                // Semester V
                Module.createModule("Praxis III", 8),
                Module.createModule("Software Engineering II", 10),
                Module.createModule("Datenbanken II", 5),
                Module.createModule("Wissensbasierte und interaktive Systeme", 5),
                Module.createModule("Consulting, technischer Vertrieb und Recht", 5),

                // Semester VI
                Module.createModule("Große Studienarbeit", 10),
                Module.createModule("Bachelorarbeit", 0, 0.2),
                Module.createModule("Kommunikations- und Netztechnik II", 5),
                Module.createModule("E-Business", 5),
                Module.createModule("Wahlmodul (KA-INF)", 5)
        ));

        return profile;
    }

    public static Profile getExample() {
        Profile profile = new Profile();

        boolean isGerman = Locale.getDefault().getLanguage().equals(Locale.GERMAN.getLanguage());

        profile.getModules().addAll(Arrays.asList(
                Module.createModule(isGerman ? "Mathematik" : "Math",
                        new Submodule("Algebra", 4),
                        new Submodule("Analysis", 4)
                ),
                Module.createModule(isGerman ? "Informatik" : "Computer Science", 5),
                Module.createModule(isGerman ? "Gesellschaftslehre" : "Social Studies",
                        new Submodule(isGerman ? "Schriftlich" : "Written", 2.5),
                        new Submodule(isGerman ? "Mündlich" : "Oral", 2.5)
                ),
                Module.createModule(isGerman ? "Biologie" : "Biology", 3),
                Module.createModule(isGerman ? "Chemie" : "Chemistry", 3),
                Module.createModule(isGerman ? "Physik" : "Physics", 3),
                Module.createModule(isGerman ? "Geschichte" : "History", 3),
                Module.createModule(isGerman ? "Bachelorarbeit" : "Bachelor's Thesis", 0, 0.2)
        ));

        return profile;
    }

    public List<Module> getModules() {
        return modules;
    }

    public void setModules(List<Module> modules) {
        this.modules = modules;
    }
}
