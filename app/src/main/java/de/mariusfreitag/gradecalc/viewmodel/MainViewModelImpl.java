package de.mariusfreitag.gradecalc.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;

import de.mariusfreitag.gradecalc.R;
import de.mariusfreitag.gradecalc.common.GradeCalculator;
import de.mariusfreitag.gradecalc.model.Profile;
import de.mariusfreitag.gradecalc.model.Submodule;

public class MainViewModelImpl extends BaseObservable implements MainViewModel {

    private final GradeCalculator gradeCalculator;
    private final Context context;

    private boolean isRefreshing = false;
    private int projectionValueIndex = 1;
    private Profile profile = null;
    private Runnable setGradesClickListener = null;

    public MainViewModelImpl(GradeCalculator gradeCalculator, Context context) {
        this.gradeCalculator = gradeCalculator;
        this.context = context;
    }

    @Override
    public boolean getIsRefreshing() {
        return isRefreshing;
    }

    @Override
    public String getCurrentGradeText() {
        if (profile == null) {
            return "";
        }

        String currentGrade = String.valueOf(gradeCalculator.calculateGradeAverage(profile.getModules()));

        if (currentGrade.equals("0.0")) {
            return "-";
        }

        if (currentGrade.length() < 3) {
            return currentGrade;
        }

        return currentGrade.substring(0, 3);
    }

    @Override
    public int getCurrentGradeColor() {
        return getGradeColor(getCurrentGradeText());

    }

    @Override
    public String getCurrentGradeAdditionalDigitsText() {
        if (profile == null) {
            return "";
        }

        String currentGrade = String.valueOf(gradeCalculator.calculateGradeAverage(profile.getModules()));

        if (currentGrade.length() <= 3 || currentGrade.toCharArray()[3] == '0') {
            return "";
        }

        if (currentGrade.length() <= 4 || currentGrade.toCharArray()[4] == '0') {
            return currentGrade.substring(3, 4);
        }

        return currentGrade.substring(3, 5);
    }

    @Override
    public String getPreviewText() {
        if (profile == null) {
            return "";
        }

        String preview = String.valueOf(gradeCalculator.projectGradeAverage(profile.getModules(), getProjectionValueIndex()));

        if (preview.equals("0.0")) {
            return "-";
        }

        if (preview.length() < 3) {
            return preview;
        }

        return preview.substring(0, 3);
    }

    @Override
    public int getPreviewColor() {
        return getGradeColor(getPreviewText());
    }

    @Override
    public int getProjectionValueIndex() {
        return projectionValueIndex;
    }

    @Override
    public void setProjectionValueIndex(int projectionValueIndex) {
        this.projectionValueIndex = projectionValueIndex;
        notifyChange();
    }

    @Override
    public String getGradeSetCountText() {
        if (profile == null) {
            return "";
        }

        return String.valueOf(
                profile.getModules().stream()
                        .flatMap((m) -> m.getSubmodules().stream())
                        .filter((s) -> s.getGradeIndex() > 0)
                        .count()
        );
    }

    @Override
    public String getGradeTotalCountText() {
        if (profile == null) {
            return "";
        }

        return String.valueOf(
                profile.getModules().stream()
                        .mapToLong((m) -> m.getSubmodules().size())
                        .sum())
                ;
    }

    @Override
    public String getCreditsSetCountText() {
        if (profile == null) {
            return "";
        }

        return String.valueOf(
                profile.getModules().stream()
                        .flatMap((m) -> m.getSubmodules().stream())
                        .filter((s) -> s.getGradeIndex() > 0)
                        .mapToDouble(Submodule::getCredits)
                        .sum()
        );
    }

    @Override
    public String getCreditsTotalCountText() {
        if (profile == null) {
            return "";
        }

        return String.valueOf(
                profile.getModules().stream()
                        .flatMap((m) -> m.getSubmodules().stream())
                        .mapToDouble(Submodule::getCredits)
                        .sum()
        );
    }

    @Override
    public void bindProfile(Profile profile) {
        isRefreshing = true;
        this.profile = profile;
        notifyChange();
        isRefreshing = false;
    }

    @Override
    public void rebindProfile() {
        bindProfile(profile);
    }

    @Override
    public void onSetGradesClick() {
        if (setGradesClickListener != null) {
            setGradesClickListener.run();
        }
    }

    @Override
    public void setSetGradesClickListener(Runnable setGradesClickListener) {
        this.setGradesClickListener = setGradesClickListener;
    }

    private int getGradeColor(String gradeString) {
        double grade = 0.0;

        try {
            grade = Double.parseDouble(gradeString);
        } catch (Exception ignored) {
        }

        if (grade < 1.0)
            return Color.BLACK;
        else if (grade < 1.1)
            return ContextCompat.getColor(context, R.color.colorGradeBest);
        else if (grade < 1.5)
            return ContextCompat.getColor(context, R.color.colorGradeVeryGood);
        else if (grade < 2.0)
            return ContextCompat.getColor(context, R.color.colorGradeGood);
        else if (grade <= 2.5)
            return ContextCompat.getColor(context, R.color.colorGradeOkay);
        else if (grade <= 3.0)
            return ContextCompat.getColor(context, R.color.colorGradeEnough);
        else if (grade < 4.0)
            return ContextCompat.getColor(context, R.color.colorGradeBad);
        else
            return ContextCompat.getColor(context, R.color.colorGradeFailed);
    }
}
