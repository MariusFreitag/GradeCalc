package de.mariusfreitag.gradecalc.viewmodel;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.graphics.drawable.Drawable;

import java.util.List;

import de.mariusfreitag.gradecalc.adapters.BindingAdapters;
import de.mariusfreitag.gradecalc.model.Module;

public interface ModuleEditableViewModel extends Observable, BindingAdapters {
    @Bindable
    String getModuleNameText();

    void setModuleNameText(String moduleNameText);

    @Bindable
    String getAdditionalInfluenceText();

    void setAdditionalInfluenceText(String additionalInfluenceText);

    @Bindable
    List<SubmoduleEditableViewModel> getSubmodules();

    void addSubmodule();

    @Bindable
    boolean getExpanded();

    void setExpanded(boolean expanded);

    void toggleExpanded();

    @Bindable({"detailsVisibility", "expanded"})
    int getDetailsVisibility();

    @Bindable({"expandButtonImage", "expanded"})
    Drawable getExpandButtonImage();

    void bindModule(Module module);
}