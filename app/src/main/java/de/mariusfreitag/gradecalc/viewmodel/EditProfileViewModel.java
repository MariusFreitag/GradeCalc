package de.mariusfreitag.gradecalc.viewmodel;

import android.databinding.Observable;

public interface EditProfileViewModel extends Observable {
    void onAddModuleClick();

    void setAddModuleClickListener(Runnable listener);
}
