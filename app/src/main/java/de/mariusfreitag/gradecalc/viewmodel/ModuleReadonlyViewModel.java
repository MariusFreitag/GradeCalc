package de.mariusfreitag.gradecalc.viewmodel;

import android.databinding.Bindable;
import android.databinding.Observable;

import java.util.List;

import de.mariusfreitag.gradecalc.adapters.BindingAdapters;
import de.mariusfreitag.gradecalc.model.Module;

public interface ModuleReadonlyViewModel extends Observable, BindingAdapters {
    @Bindable
    String getModuleNameText();

    @Bindable
    String getAdditionalInfluenceText();

    @Bindable
    List<SubmoduleReadonlyViewModel> getSubmodules();

    @Bindable({"additionalInfluenceVisibility", "additionalInfluenceText"})
    int getAdditionalInfluenceVisibility();

    void bindModule(Module module);
}