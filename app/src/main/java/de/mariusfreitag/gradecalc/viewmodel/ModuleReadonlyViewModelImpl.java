package de.mariusfreitag.gradecalc.viewmodel;

import android.databinding.BaseObservable;
import android.view.View;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import de.mariusfreitag.gradecalc.model.Module;

public class ModuleReadonlyViewModelImpl extends BaseObservable implements ModuleReadonlyViewModel {

    private Module module = null;

    @Override
    public String getModuleNameText() {
        if (module == null) {
            return "";
        }

        return module.getName();
    }

    @Override
    public String getAdditionalInfluenceText() {
        if (module == null) {
            return "";
        }

        return String.valueOf(module.getAdditionalInfluence());
    }

    @Override
    public List<SubmoduleReadonlyViewModel> getSubmodules() {
        if (module == null) {
            return new LinkedList<>();
        }

        return module.getSubmodules().stream().map((submodule -> {
            SubmoduleReadonlyViewModel submoduleModel = new SubmoduleReadonlyViewModelImpl();
            submoduleModel.bindSubmodule(submodule);
            return submoduleModel;
        })).collect(Collectors.toList());
    }

    @Override
    public int getAdditionalInfluenceVisibility() {
        return module.getAdditionalInfluence() > 0 ? View.VISIBLE : View.GONE;
    }

    @Override
    public void bindModule(Module module) {
        this.module = module;
    }
}
