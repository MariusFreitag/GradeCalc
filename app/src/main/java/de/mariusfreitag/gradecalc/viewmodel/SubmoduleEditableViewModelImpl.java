package de.mariusfreitag.gradecalc.viewmodel;

import android.databinding.BaseObservable;
import android.view.View;

import de.mariusfreitag.gradecalc.BR;
import de.mariusfreitag.gradecalc.model.Module;
import de.mariusfreitag.gradecalc.model.Submodule;

public class SubmoduleEditableViewModelImpl extends BaseObservable implements SubmoduleEditableViewModel {
    private Module module;
    private Submodule submodule;
    private String rawSubmoduleCreditsText = null;

    @Override
    public String getSubmoduleNameText() {
        if (submodule == null) {
            return "";
        }

        return submodule.getName();
    }

    @Override
    public void setSubmoduleNameText(String submoduleNameText) {
        if (submodule == null) {
            return;
        }

        submodule.setName(submoduleNameText);
        notifyPropertyChanged(BR.submoduleNameText);
    }

    @Override
    public String getSubmoduleCreditsText() {
        if (submodule == null) {
            return "";
        }

        if (rawSubmoduleCreditsText != null) {
            return rawSubmoduleCreditsText;
        }

        return String.valueOf(submodule.getCredits());
    }

    @Override
    public void setSubmoduleCreditsText(String submoduleCreditsText) {
        if (submodule == null) {
            return;
        }

        rawSubmoduleCreditsText = submoduleCreditsText;

        try {
            submodule.setCredits(Double.parseDouble(submoduleCreditsText.replace(',', '.')));
        } catch (Exception e) {
            submodule.setCredits(0);
        }

        notifyPropertyChanged(BR.submoduleCreditsText);
    }

    @Override
    public int getRemoveButtonVisibility() {
        if (module == null) {
            return View.GONE;
        }

        return module.getSubmodules().size() > 1 ? View.VISIBLE : View.GONE;
    }

    @Override
    public void removeSubmodule() {
        if (module == null || submodule == null) {
            return;
        }

        module.getSubmodules().remove(submodule);
        submodule = null;
        notifyPropertyChanged(BR.submodules);
    }

    @Override
    public void bindSubmoduleModule(Module module, Submodule submodule) {
        this.module = module;
        this.submodule = submodule;
        notifyChange();
    }
}
