package de.mariusfreitag.gradecalc.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Observable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.View;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import de.mariusfreitag.gradecalc.BR;
import de.mariusfreitag.gradecalc.R;
import de.mariusfreitag.gradecalc.model.Module;
import de.mariusfreitag.gradecalc.model.Submodule;

public class ModuleEditableViewModelImpl extends BaseObservable implements ModuleEditableViewModel {
    private final Context context;
    private Module module = null;
    private boolean expanded = false;
    private String rawAdditionalInfluenceText = null;

    public ModuleEditableViewModelImpl(Context context) {
        this.context = context;
    }

    @Override
    public String getModuleNameText() {
        if (module == null) {
            return "";
        }

        return module.getName();
    }

    @Override
    public void setModuleNameText(String moduleNameText) {
        if (module == null) {
            return;
        }

        module.setName(moduleNameText);
        notifyPropertyChanged(BR.moduleNameText);
    }

    @Override
    public String getAdditionalInfluenceText() {
        if (module == null) {
            return "";
        }

        if (rawAdditionalInfluenceText != null) {
            return rawAdditionalInfluenceText;
        }

        return String.valueOf(module.getAdditionalInfluence());
    }

    @Override
    public void setAdditionalInfluenceText(String additionalInfluenceText) {
        if (module == null) {
            return;
        }

        rawAdditionalInfluenceText = additionalInfluenceText;

        try {
            double additionalInfluence = Double.parseDouble(additionalInfluenceText.replace(',', '.'));

            if (additionalInfluence > 1) {
                additionalInfluence = additionalInfluence - (int) additionalInfluence;
                rawAdditionalInfluenceText = null;
            }

            module.setAdditionalInfluence(additionalInfluence);
        } catch (Exception e) {
            module.setAdditionalInfluence(0);
        }

        notifyPropertyChanged(BR.additionalInfluenceText);
    }

    @Override
    public List<SubmoduleEditableViewModel> getSubmodules() {
        if (module == null) {
            return new LinkedList<>();
        }

        return module.getSubmodules().stream().map((submodule -> {
            SubmoduleEditableViewModel submoduleModel = new SubmoduleEditableViewModelImpl();
            submoduleModel.bindSubmoduleModule(module, submodule);
            submoduleModel.addOnPropertyChangedCallback(new OnPropertyChangedCallback() {
                @Override
                public void onPropertyChanged(Observable sender, int propertyId) {
                    if (propertyId == BR.submodules) {
                        notifyPropertyChanged(BR.submodules);
                    }
                }
            });
            return submoduleModel;
        })).collect(Collectors.toList());
    }

    @Override
    public void addSubmodule() {
        if (module == null) {
            return;
        }

        module.getSubmodules().add(new Submodule("", 0));
        notifyPropertyChanged(BR.submodules);
    }

    @Override
    public boolean getExpanded() {
        return expanded;
    }

    @Override
    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
        notifyPropertyChanged(BR.expanded);
    }

    @Override
    public void toggleExpanded() {
        expanded = !expanded;
        notifyPropertyChanged(BR.expanded);
    }

    @Override
    public int getDetailsVisibility() {
        return expanded ? View.VISIBLE : View.GONE;
    }

    @Override
    public Drawable getExpandButtonImage() {
        return ContextCompat.getDrawable(context, expanded ? R.drawable.ic_expand_less_black_24dp : R.drawable.ic_expand_more_black_24dp);
    }

    @Override
    public void bindModule(Module module) {
        this.module = module;
        notifyChange();
    }
}
