package de.mariusfreitag.gradecalc.viewmodel;

import android.databinding.BaseObservable;

public class SetGradesViewModelImpl extends BaseObservable implements SetGradesViewModel {
    private Runnable editProfileClickListener;

    @Override
    public void onEditProfileClick() {
        if (editProfileClickListener != null) {
            editProfileClickListener.run();
        }
    }

    @Override
    public void setEditProfileClickListener(Runnable listener) {
        this.editProfileClickListener = listener;
    }
}
