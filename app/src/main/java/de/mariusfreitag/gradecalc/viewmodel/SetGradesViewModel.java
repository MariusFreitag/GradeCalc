package de.mariusfreitag.gradecalc.viewmodel;

import android.databinding.Observable;

public interface SetGradesViewModel extends Observable {
    void onEditProfileClick();

    void setEditProfileClickListener(Runnable listener);
}
