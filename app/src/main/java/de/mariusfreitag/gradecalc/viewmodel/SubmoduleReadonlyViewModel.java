package de.mariusfreitag.gradecalc.viewmodel;

import android.databinding.Bindable;
import android.databinding.Observable;

import de.mariusfreitag.gradecalc.model.Submodule;

public interface SubmoduleReadonlyViewModel extends Observable {
    @Bindable
    String getSubmoduleNameText();

    @Bindable
    String getSubmoduleCreditsText();

    @Bindable
    int getGradeIndex();

    void setGradeIndex(int gradeIndex);

    void bindSubmodule(Submodule submodule);
}
