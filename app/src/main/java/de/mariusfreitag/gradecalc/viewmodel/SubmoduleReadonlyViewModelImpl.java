package de.mariusfreitag.gradecalc.viewmodel;

import android.databinding.BaseObservable;

import de.mariusfreitag.gradecalc.BR;
import de.mariusfreitag.gradecalc.model.Submodule;

public class SubmoduleReadonlyViewModelImpl extends BaseObservable implements SubmoduleReadonlyViewModel {
    private Submodule submodule;

    @Override
    public String getSubmoduleNameText() {
        if (submodule == null) {
            return "";
        }

        return submodule.getName();
    }

    @Override
    public String getSubmoduleCreditsText() {
        if (submodule == null) {
            return "";
        }

        return String.valueOf(submodule.getCredits());
    }

    @Override
    public int getGradeIndex() {
        if (submodule == null) {
            return 0;
        }

        return submodule.getGradeIndex();
    }

    @Override
    public void setGradeIndex(int gradeIndex) {
        if (submodule == null) {
            return;
        }

        submodule.setGradeIndex(gradeIndex);
        notifyPropertyChanged(BR.gradeIndex);
    }

    @Override
    public void bindSubmodule(Submodule submodule) {
        this.submodule = submodule;
    }
}
