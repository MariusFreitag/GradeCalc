package de.mariusfreitag.gradecalc.viewmodel;

import android.databinding.Bindable;
import android.databinding.Observable;

import de.mariusfreitag.gradecalc.model.Module;
import de.mariusfreitag.gradecalc.model.Submodule;

public interface SubmoduleEditableViewModel extends Observable {
    @Bindable
    String getSubmoduleNameText();

    void setSubmoduleNameText(String submoduleNameText);

    @Bindable
    String getSubmoduleCreditsText();

    void setSubmoduleCreditsText(String submoduleCreditsText);

    @Bindable
    int getRemoveButtonVisibility();

    void removeSubmodule();

    void bindSubmoduleModule(Module module, Submodule submodule);
}
