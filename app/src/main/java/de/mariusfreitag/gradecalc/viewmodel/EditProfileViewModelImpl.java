package de.mariusfreitag.gradecalc.viewmodel;

import android.databinding.BaseObservable;

public class EditProfileViewModelImpl extends BaseObservable implements EditProfileViewModel {
    private Runnable addModuleClickListener;

    @Override
    public void onAddModuleClick() {
        if (addModuleClickListener != null) {
            addModuleClickListener.run();
        }
    }

    @Override
    public void setAddModuleClickListener(Runnable listener) {
        this.addModuleClickListener = listener;
    }
}
