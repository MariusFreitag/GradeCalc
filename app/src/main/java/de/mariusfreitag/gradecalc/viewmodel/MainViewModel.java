package de.mariusfreitag.gradecalc.viewmodel;

import android.databinding.Bindable;
import android.databinding.Observable;

import de.mariusfreitag.gradecalc.model.Profile;

public interface MainViewModel extends Observable {
    @Bindable
    boolean getIsRefreshing();

    @Bindable
    String getCurrentGradeText();

    @Bindable({"currentGradeColor", "currentGradeText"})
    int getCurrentGradeColor();

    @Bindable
    String getCurrentGradeAdditionalDigitsText();

    @Bindable
    String getPreviewText();

    @Bindable({"previewColor", "previewText"})
    int getPreviewColor();

    @Bindable
    int getProjectionValueIndex();

    void setProjectionValueIndex(int projectionValueIndex);

    @Bindable
    String getGradeSetCountText();

    @Bindable
    String getGradeTotalCountText();

    @Bindable
    String getCreditsSetCountText();

    @Bindable
    String getCreditsTotalCountText();

    void bindProfile(Profile profile);

    void rebindProfile();

    void onSetGradesClick();

    void setSetGradesClickListener(Runnable listener);
}
