package de.mariusfreitag.gradecalc.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import de.mariusfreitag.gradecalc.R;
import de.mariusfreitag.gradecalc.common.FileAccessFailedDialog;
import de.mariusfreitag.gradecalc.common.GradeCalculator;
import de.mariusfreitag.gradecalc.common.GradeIndexResolver;
import de.mariusfreitag.gradecalc.common.Storage;
import de.mariusfreitag.gradecalc.databinding.ActivityMainBinding;
import de.mariusfreitag.gradecalc.viewmodel.MainViewModel;
import de.mariusfreitag.gradecalc.viewmodel.MainViewModelImpl;

public class MainActivity extends AppCompatActivity {
    private static final int PERMISSION_REQUEST_REFRESH_AFTER_GRANT = 123;
    private MainViewModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        model = new MainViewModelImpl(new GradeCalculator(new GradeIndexResolver(this)), this);
        model.setSetGradesClickListener(() -> startActivity(new Intent(getApplicationContext(), SetGradesActivity.class)));

        SwipeRefreshLayout layout = (SwipeRefreshLayout) getLayoutInflater().inflate(R.layout.activity_main, null);
        ActivityMainBinding binding = ActivityMainBinding.bind(layout);
        binding.setModel(model);

        setContentView(layout);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.info:
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage(R.string.info_text)
                        .setPositiveButton(getString(R.string.ok), null)
                        .show();
                break;
            case R.id.refresh:
                calculate();
                break;
            case R.id.close:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        calculate();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_REQUEST_REFRESH_AFTER_GRANT
                && permissions.length > 0
                && permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                calculate();
            } else {
                calculate();
            }
        }
    }

    private void calculate() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_REFRESH_AFTER_GRANT);
        } else {
            try {
                model.bindProfile(Storage.getProfile());
            } catch (Storage.StorageException e) {
                e.printStackTrace();
                new FileAccessFailedDialog().show(this);
            }
        }
    }
}
