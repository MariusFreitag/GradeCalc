package de.mariusfreitag.gradecalc.activities;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import java.util.Objects;

import de.mariusfreitag.gradecalc.R;
import de.mariusfreitag.gradecalc.adapters.EditProfileRecyclerAdapter;
import de.mariusfreitag.gradecalc.common.FileAccessFailedDialog;
import de.mariusfreitag.gradecalc.common.Storage;
import de.mariusfreitag.gradecalc.databinding.ActivityEditProfileBinding;
import de.mariusfreitag.gradecalc.model.Module;
import de.mariusfreitag.gradecalc.model.Profile;
import de.mariusfreitag.gradecalc.viewmodel.EditProfileViewModel;
import de.mariusfreitag.gradecalc.viewmodel.EditProfileViewModelImpl;

public class EditProfileActivity extends AppCompatActivity {

    private EditProfileRecyclerAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Enable action bar with back button
        Objects.requireNonNull(getSupportActionBar()).setTitle(getString(R.string.edit_profile));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Inflate layout
        RelativeLayout layout = (RelativeLayout) getLayoutInflater().inflate(R.layout.activity_edit_profile, null);

        // Initialize RecyclerView
        recyclerAdapter = new EditProfileRecyclerAdapter();
        RecyclerView recyclerView = layout.findViewById(R.id.recyclerView);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(null));

        // Bind view model
        EditProfileViewModel model = new EditProfileViewModelImpl();
        model.setAddModuleClickListener(() -> recyclerAdapter.addModule(Module.createModule("", 0)));
        ActivityEditProfileBinding binding = ActivityEditProfileBinding.bind(layout);
        binding.setModel(model);

        setContentView(layout);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_edit_profile_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.import_example:
                recyclerAdapter.setModules(Profile.getExample().getModules());
                break;
            case R.id.import_tinf:
                recyclerAdapter.setModules(Profile.getTinf().getModules());
                break;
            case R.id.clear:
                recyclerAdapter.setModules(Profile.getDefault().getModules());
                break;
            case android.R.id.home:
                this.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            Profile profile = Storage.getProfile();
            profile.setModules(recyclerAdapter.getModules());
            Storage.setProfile(profile);
        } catch (Storage.StorageException e) {
            e.printStackTrace();
            new FileAccessFailedDialog().show(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            recyclerAdapter.setModules(Storage.getProfile().getModules());
        } catch (Storage.StorageException e) {
            e.printStackTrace();
            new FileAccessFailedDialog().show(this);
        }

        // Propose to import an example profile when not asked yet
        if (!Storage.isImportAsked()) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.question_import)
                    .setPositiveButton(R.string.no, null)
                    .setNegativeButton(R.string.yes_example, (dialog, which) -> recyclerAdapter.setModules(Profile.getExample().getModules()))
                    .setNeutralButton(R.string.yes_tinf, (dialog, which) -> recyclerAdapter.setModules(Profile.getTinf().getModules()))
                    .show();

            Storage.setImportAsked();
        }
    }
}
