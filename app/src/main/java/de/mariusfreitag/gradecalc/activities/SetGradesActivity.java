package de.mariusfreitag.gradecalc.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import java.util.Objects;

import de.mariusfreitag.gradecalc.R;
import de.mariusfreitag.gradecalc.adapters.SetGradesRecyclerAdapter;
import de.mariusfreitag.gradecalc.common.FileAccessFailedDialog;
import de.mariusfreitag.gradecalc.common.Storage;
import de.mariusfreitag.gradecalc.databinding.ActivitySetGradesBinding;
import de.mariusfreitag.gradecalc.model.Module;
import de.mariusfreitag.gradecalc.model.Profile;
import de.mariusfreitag.gradecalc.model.Submodule;
import de.mariusfreitag.gradecalc.viewmodel.SetGradesViewModel;
import de.mariusfreitag.gradecalc.viewmodel.SetGradesViewModelImpl;

public class SetGradesActivity extends AppCompatActivity {

    private SetGradesRecyclerAdapter recyclerAdapter;
    private Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Enable action bar with back button
        Objects.requireNonNull(getSupportActionBar()).setTitle(getString(R.string.set_grades));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Inflate layout
        RelativeLayout layout = (RelativeLayout) getLayoutInflater().inflate(R.layout.activity_set_grades, null);

        // Initialize RecyclerView
        recyclerAdapter = new SetGradesRecyclerAdapter();
        RecyclerView recyclerView = layout.findViewById(R.id.recyclerView);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(null));

        // Bind view model
        SetGradesViewModel model = new SetGradesViewModelImpl();
        model.setEditProfileClickListener(() -> startActivity(new Intent(getApplicationContext(), EditProfileActivity.class)));
        ActivitySetGradesBinding binding = ActivitySetGradesBinding.bind(layout);
        binding.setModel(model);

        setContentView(layout);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_set_grades_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.reset:
                for (Module module : profile.getModules()) {
                    for (Submodule submodule : module.getSubmodules()) {
                        submodule.setGradeIndex(0);
                    }
                }
                break;
            case android.R.id.home:
                this.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            // Save profile changes
            Storage.setProfile(this.profile);
        } catch (Storage.StorageException e) {
            e.printStackTrace();
            new FileAccessFailedDialog().show(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            // Get profile
            this.profile = Storage.getProfile();
        } catch (Storage.StorageException e) {
            e.printStackTrace();
            new FileAccessFailedDialog().show(this);
        }

        // Redirect to edit profile if no modules defined
        if (this.profile.getModules().size() < 1) {
            redirectToEditProfile();
        }

        recyclerAdapter.setModules(this.profile.getModules());
    }

    private void redirectToEditProfile() {
        new AlertDialog.Builder(this)
                .setMessage(getString(R.string.question_no_modules))
                .setPositiveButton(getString(R.string.yes), (dialog, which) -> startActivity(new Intent(getApplicationContext(), EditProfileActivity.class)))
                .setNegativeButton(R.string.no, null)
                .show();
    }
}
