package de.mariusfreitag.gradecalc.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.LinkedList;
import java.util.List;

import de.mariusfreitag.gradecalc.R;
import de.mariusfreitag.gradecalc.databinding.LayoutModuleReadonlyBinding;
import de.mariusfreitag.gradecalc.model.Module;
import de.mariusfreitag.gradecalc.viewmodel.ModuleReadonlyViewModel;
import de.mariusfreitag.gradecalc.viewmodel.ModuleReadonlyViewModelImpl;

public class SetGradesRecyclerAdapter extends RecyclerView.Adapter<SetGradesRecyclerAdapter.ViewHolder> {

    private List<Module> modules = new LinkedList<>();

    public void setModules(List<Module> modules) {
        this.modules = modules;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardView card = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_module_readonly, parent, false);
        return new ViewHolder(card);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindModule(modules.get(position));
    }

    @Override
    public int getItemCount() {
        return modules.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final CardView card;
        private final LayoutModuleReadonlyBinding binding;

        ViewHolder(CardView itemView) {
            super(itemView);
            card = itemView;
            binding = LayoutModuleReadonlyBinding.bind(card);
        }

        void bindModule(Module module) {
            ModuleReadonlyViewModel model = new ModuleReadonlyViewModelImpl();
            model.bindModule(module);
            binding.setModel(model);
        }
    }
}
