package de.mariusfreitag.gradecalc.adapters;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

public interface BindingAdapters {
    @BindingAdapter({"models", "layout"})
    static <T> void bindSubmodules(ViewGroup viewGroup, List<T> models, int layoutId) {
        viewGroup.removeAllViews();

        if (models != null) {
            LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            for (final T model : models) {
                ViewDataBinding binding = DataBindingUtil
                        .inflate(inflater, layoutId, viewGroup, true);

                binding.setVariable(com.android.databinding.library.baseAdapters.BR.model, model);
            }
        }
    }
}
