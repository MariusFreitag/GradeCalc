package de.mariusfreitag.gradecalc.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.mariusfreitag.gradecalc.R;
import de.mariusfreitag.gradecalc.databinding.LayoutModuleEditableBinding;
import de.mariusfreitag.gradecalc.model.Module;
import de.mariusfreitag.gradecalc.viewmodel.ModuleEditableViewModel;
import de.mariusfreitag.gradecalc.viewmodel.ModuleEditableViewModelImpl;

public class EditProfileRecyclerAdapter extends RecyclerView.Adapter<EditProfileRecyclerAdapter.ViewHolder> implements ItemTouchHelperAdapter {

    private List<Module> modules = new LinkedList<>();
    private RecyclerView recyclerView;
    private ItemTouchHelper itemTouchHelper;

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        this.recyclerView = recyclerView;

        itemTouchHelper = new ItemTouchHelper(new ModuleItemTouchHelperCallback(this));
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    public List<Module> getModules() {
        return modules;
    }

    public void setModules(List<Module> modules) {
        this.modules = modules;
        notifyDataSetChanged();
    }

    public void addModule(Module module) {
        getModules().add(module);
        notifyItemInserted(getItemCount() - 1);
        recyclerView.smoothScrollToPosition(getItemCount() - 1);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardView card = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_module_editable, parent, false);
        return new ViewHolder(card);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindModule(modules.get(position));
    }

    @Override
    public int getItemCount() {
        return modules.size();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {

        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(modules, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(modules, i, i - 1);
            }
        }

        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onItemDismiss(int position) {
        modules.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final CardView card;
        private final LayoutModuleEditableBinding binding;

        ViewHolder(CardView itemView) {
            super(itemView);
            card = itemView;
            binding = LayoutModuleEditableBinding.bind(card);

            card.findViewById(R.id.dragHandle).setOnTouchListener((v, event) -> {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    itemTouchHelper.startDrag(ViewHolder.this);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    v.performClick();
                }
                return false;
            });
        }

        void bindModule(Module module) {
            ModuleEditableViewModel model = new ModuleEditableViewModelImpl(card.getContext());
            model.bindModule(module);
            binding.setModel(model);
        }
    }
}
