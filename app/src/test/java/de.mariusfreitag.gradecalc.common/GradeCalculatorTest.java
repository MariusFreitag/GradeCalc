package de.mariusfreitag.gradecalc.common;

import org.junit.Before;
import org.junit.Test;

import de.mariusfreitag.gradecalc.model.Profile;
import de.mariusfreitag.gradecalc.model.WeightedValue;

import static org.junit.Assert.assertEquals;

public class GradeCalculatorTest {
    private final double DOUBLE_DELTA = 0.000001;
    private GradeCalculator gradeCalculator;

    @Before
    public void before() {
        // Create mock index resolver (11 -> 1.1)
        this.gradeCalculator = new GradeCalculator(index -> index / 10.0);
    }

    @Test
    public void testCutOffDecimals() {
        assertEquals(1, gradeCalculator.cutOffDecimals(1), DOUBLE_DELTA);
        assertEquals(1.1, gradeCalculator.cutOffDecimals(1.19), DOUBLE_DELTA);
        assertEquals(1.1, gradeCalculator.cutOffDecimals(1.1987), DOUBLE_DELTA);
        assertEquals(1.1, gradeCalculator.cutOffDecimals(1.11), DOUBLE_DELTA);
        assertEquals(1.1, gradeCalculator.cutOffDecimals(1.12345), DOUBLE_DELTA);
        assertEquals(0, gradeCalculator.cutOffDecimals(0), DOUBLE_DELTA);
        assertEquals(1.7, gradeCalculator.cutOffDecimals(1.7), DOUBLE_DELTA);
        assertEquals(2.3, gradeCalculator.cutOffDecimals(2.31), DOUBLE_DELTA);
    }

    @Test
    public void testCalculateModuleGrade() {
        // Test 1: Module with two submodules
        Profile profile = Profile.getExample();

        profile.getModules().get(0).getSubmodules().get(0).setGradeIndex(15);
        profile.getModules().get(0).getSubmodules().get(1).setGradeIndex(20);
        WeightedValue weightedValue = gradeCalculator.calculateModuleGrade(profile.getModules().get(0));

        assertEquals(4.0 + 4.0, weightedValue.getWeight(), DOUBLE_DELTA);
        assertEquals(gradeCalculator.cutOffDecimals((1.5 * 4.0 + 2.0 * 4.0) / 8.0), weightedValue.getValue(), DOUBLE_DELTA);


        // Test 2: Module with zero credits
        profile = Profile.getExample();

        profile.getModules().get(7).getSubmodules().get(0).setGradeIndex(15);
        weightedValue = gradeCalculator.calculateModuleGrade(profile.getModules().get(7));

        assertEquals(0, weightedValue.getWeight(), DOUBLE_DELTA);
        assertEquals(1.5, weightedValue.getValue(), DOUBLE_DELTA);


        // Test 3: Module with one submodule
        profile = Profile.getExample();

        profile.getModules().get(0).getSubmodules().get(0).setGradeIndex(16);
        weightedValue = gradeCalculator.calculateModuleGrade(profile.getModules().get(0));

        assertEquals(4, weightedValue.getWeight(), DOUBLE_DELTA);
        assertEquals(1.6, weightedValue.getValue(), DOUBLE_DELTA);
    }

    @Test
    public void testCalculateGradeAverage() {
        // Test 1: One module with two submodules and one normal module
        Profile profile = Profile.getExample();

        profile.getModules().get(0).getSubmodules().get(0).setGradeIndex(11);
        profile.getModules().get(0).getSubmodules().get(1).setGradeIndex(13);
        profile.getModules().get(1).getSubmodules().get(0).setGradeIndex(16);

        assertEquals((1.1 * 4.0 + 1.3 * 4.0 + 1.6 * 5.0) / 13.0, gradeCalculator.calculateGradeAverage(profile.getModules()), DOUBLE_DELTA);


        // Test 2: One normal module and one bachelor thesis
        profile = Profile.getExample();

        profile.getModules().get(1).getSubmodules().get(0).setGradeIndex(16);
        profile.getModules().get(7).getSubmodules().get(0).setGradeIndex(22);

        assertEquals(1.6 * 0.8 + 2.2 * 0.2, gradeCalculator.calculateGradeAverage(profile.getModules()), DOUBLE_DELTA);


        // Test 3: Only one bachelor thesis
        profile = Profile.getExample();

        profile.getModules().get(7).getSubmodules().get(0).setGradeIndex(22);

        assertEquals(2.2, gradeCalculator.calculateGradeAverage(profile.getModules()), DOUBLE_DELTA);


        // Test 4: No modules
        profile = Profile.getExample();

        assertEquals(gradeCalculator.calculateGradeAverage(profile.getModules()), 0, DOUBLE_DELTA);


        // Test 5: Two modules
        profile = Profile.getExample();

        profile.getModules().get(0).getSubmodules().get(0).setGradeIndex(16);
        profile.getModules().get(1).getSubmodules().get(0).setGradeIndex(14);

        assertEquals((1.6 * 4.0 + 1.4 * 5.0) / 9.0, gradeCalculator.calculateGradeAverage(profile.getModules()), DOUBLE_DELTA);
    }

    @Test
    public void testProjectGradeAverage() {
        // Test 1: One module with two submodules and one normal module
        Profile profile = Profile.getExample();

        profile.getModules().get(0).getSubmodules().get(0).setGradeIndex(11);
        profile.getModules().get(0).getSubmodules().get(1).setGradeIndex(13);
        profile.getModules().get(1).getSubmodules().get(0).setGradeIndex(16);

        assertEquals((gradeCalculator.cutOffDecimals((1.1 * 4.0 + 1.3 * 4.0 + 1.6 * 5.0 + 2.0 * 17.0) / 30) * 0.8) + 2.0 * 0.2, gradeCalculator.projectGradeAverage(profile.getModules(), 20), DOUBLE_DELTA);


        // Test 2: One normal module and one bachelor thesis
        profile = Profile.getExample();

        profile.getModules().get(1).getSubmodules().get(0).setGradeIndex(16);
        profile.getModules().get(7).getSubmodules().get(0).setGradeIndex(32);

        assertEquals((gradeCalculator.cutOffDecimals((1.6 * 5.0 + 2.0 * 25.0) / 30) * 0.8) + 3.2 * 0.2, gradeCalculator.projectGradeAverage(profile.getModules(), 20), DOUBLE_DELTA);


        // Test 3: Only one bachelor thesis
        profile = Profile.getExample();

        profile.getModules().get(7).getSubmodules().get(0).setGradeIndex(22);

        assertEquals(2.0 * 0.8 + 2.2 * 0.2, gradeCalculator.projectGradeAverage(profile.getModules(), 20), DOUBLE_DELTA);


        // Test 4: No modules
        profile = Profile.getExample();

        assertEquals(2.0, gradeCalculator.projectGradeAverage(profile.getModules(), 20), DOUBLE_DELTA);


        // Test 5: Two modules
        profile = Profile.getExample();

        profile.getModules().get(0).getSubmodules().get(0).setGradeIndex(16);
        profile.getModules().get(1).getSubmodules().get(0).setGradeIndex(14);

        assertEquals((gradeCalculator.cutOffDecimals((1.6 * 4.0 + 1.4 * 5.0 + 2.0 * 21.0) / 30) * 0.8) + 2.0 * 0.2, gradeCalculator.projectGradeAverage(profile.getModules(), 20), DOUBLE_DELTA);
    }
}
